#include <iostream>
#include <vector>

void MassResoCal(){
    
    // Truth Mass Range and mass Point
    int      BinNumber = 200;
    int      xMasslow = 130;
    int      xMassHigh = 6000;
    float    logMassLow = log10(xMasslow);
    float    logMassHigh = log10(xMassHigh);
    float    binwidth = (logMassHigh-logMassLow)/BinNumber;
    std::vector<double> MassRangeVec;
    MassRangeVec.reserve(BinNumber+1);
    for(int i=0;i<=BinNumber;i++)
    {
        MassRangeVec.push_back(pow(10,logMassLow+i*binwidth));
    }
    
    // Mass Resolution Range For simutaneously fit (variable)
    int    MReso_ee_bin = 200;
    float  MReso_ee_xlow = -0.2;
    float  MReso_ee_xhigh = 0.1; 
    int    MReso_uu_bin = 750;
    float  MReso_uu_xlow = -0.75; 
    float  MReso_uu_xhigh = 0.75;
    
    TDirectoryFile* Direct;
    TTree* tree;

    TFile* file1 = new TFile("/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/LeptonPlusJet/LJSkim_v4.1b/Run3_Zjets_highmass_Sherpa2214_merged.root");
    if ((bool)file1->Get("LJAlgo")){
        Direct = (TDirectoryFile*)file1->Get("LJAlgo");
        Direct->cd();
        if ((bool)Direct->Get("nominal")){
            tree = (TTree*)Direct->Get("nominal");
        }else{
            std::cout << " =========[Error]======== : Cannot find the tree in the directory! " << std::endl;
        }
    }else{
        std::cout << "  ========[Error]======== : Cannot find the tree directory! " << std::endl;
    }
    
    Float_t    Truth_dilepton_M;
    Float_t    Reco_dilepton_M;
    Int_t      N_electrons;
    Int_t      N_muons;
    Float_t    W_reco;
    bool       IsMuonChannel;
    bool       IsElectronChannel;
    float      MassResoValue;
    std::vector<TH1F*> MassResoHistVec_ee;
    std::vector<TH1F*> MassResoHistVec_uu;
    MassResoHistVec_uu.reserve(BinNumber);
    MassResoHistVec_ee.reserve(BinNumber);
    for(int i=0;i<=BinNumber;i++)
    {
        MassResoHistVec_ee.push_back(new TH1F(("MassResoHist_ee_" + std::to_string(i)).c_str(),"",MReso_ee_bin,MReso_ee_xlow, MReso_ee_xhigh));
        MassResoHistVec_uu.push_back(new TH1F(("MassResoHist_uu_" + std::to_string(i)).c_str(),"",MReso_uu_bin,MReso_uu_xlow, MReso_uu_xhigh));
    }

    tree->SetBranchAddress("n_signal_el", &N_electrons);
    tree->SetBranchAddress("n_signal_mu", &N_muons);
    tree->SetBranchAddress("dilepton_m", &Reco_dilepton_M);
    tree->SetBranchAddress("born_dilepton_m", &Truth_dilepton_M);
    tree->SetBranchAddress("weight_norm", &W_reco);
    
    for(int entry=0; entry<tree->GetEntries(); entry++){
        tree->GetEntry(entry);
        IsMuonChannel = (bool)(N_muons == 2);
        IsElectronChannel = (bool)(N_electrons == 2);
        MassResoValue = (Reco_dilepton_M - Truth_dilepton_M)/Truth_dilepton_M;
        if (entry % 50000 == 0){
            std::cout << "Readout: " << entry << ", " << tree->GetEntries() << std::endl;
        }
        for(int index=0;index<BinNumber;index++){
            if (MassRangeVec[index] < Truth_dilepton_M && Truth_dilepton_M < MassRangeVec[index+1]){
                if (IsMuonChannel){
                    MassResoHistVec_uu.at(index)->Fill(MassResoValue,W_reco);
                } 
                if (IsElectronChannel){
                    MassResoHistVec_ee.at(index)->Fill(MassResoValue,W_reco);
                }
            }
        }
    }
    
    TFile* Outfile_ee = new TFile("MassResoHist_ee.root","RECREATE");
    Outfile_ee->cd();
    for (int i = 0; i<BinNumber; i++){
        MassResoHistVec_ee.at(i)->Write();
    }
    Outfile_ee->Close();

    TFile* Outfile_uu = new TFile("MassResoHist_uu.root","RECREATE");
    Outfile_uu->cd();
    for (int i = 0; i<BinNumber; i++){
        MassResoHistVec_uu.at(i)->Write();
    }
    Outfile_uu->Close();



}
