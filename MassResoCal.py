from ROOT import *
import configparser
from math import *

config = configparser.ConfigParser()
config.read("Parameter.ini")

# Truth Mass Range and mass Point
BinNumber = int(config['OverallPara']['bins'])
TruthM_xLow = int(config['OverallPara']['xmin'])
TruthM_xHigh = int(config['OverallPara']['xmax']) 
logxmin = log10(TruthM_xLow)
logxmax = log10(TruthM_xHigh)
binwidth = (logxmax-logxmin)/BinNumber
xRangeList = (BinNumber+1)*[0.]
TruthMassCenterList = BinNumber *[0.]
xRangeList[0] = TruthM_xLow
for i in range(BinNumber):
    xRangeList[i+1] = pow(10,logxmin + (i+1)*binwidth)
    TruthMassCenterList[i] = pow(10,logxmin + (i+0.5)*binwidth)

# Mass Resolution Range For simutaneously fit (variable)
MReso_ee_bin = int(config['MResolPara']['mresol_ee_bin']) 
MReso_ee_xlow = float(config['MResolPara']['mresol_ee_low']) 
MReso_ee_xhigh = float(config['MResolPara']['mresol_ee_high']) 
MReso_uu_bin = int(config['MResolPara']['mresol_mm_bin']) 
MReso_uu_xlow = float(config['MResolPara']['mresol_mm_low']) 
MReso_uu_xhigh = float(config['MResolPara']['mresol_mm_high'])

# HistList for ee and mumu channel (200 hists for histList)
MassResoHistList_ee = []
MassResoHistList_uu = []
for i in range(BinNumber):
    MassResoHistList_ee.append(TH1F("MassResoHist_ee_{}".format(i),"",MReso_ee_bin, MReso_ee_xlow, MReso_ee_xhigh))
    MassResoHistList_uu.append(TH1F("MassResoHist_uu_{}".format(i),"",MReso_uu_bin, MReso_uu_xlow, MReso_uu_xhigh))

Inputfile = "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/LeptonPlusJet/LJSkim_v4.1b/Run3_Zjets_highmass_Sherpa2214_merged.root"
file = TFile.Open(Inputfile)
Direct = file.Get("LJAlgo")
Direct.cd()
tree = Direct.Get("nominal")
for entry in range(tree.GetEntries()):
    tree.GetEntry(entry)
    if (entry % 50000) == 0:
        print("Readout: ",entry, tree.GetEntries())
    Truth_dilepton_M = tree.born_dilepton_m
    Reco_dilepton_M = tree.dilepton_m
    N_electrons = tree.n_signal_el
    N_muons = tree.n_signal_mu
    W_reco = tree.weight_norm    # Luminosity : 27239.9
    IsMuonChannel = (bool)(N_muons==2)
    IsElectronChannel = (bool)(N_electrons==2)
    MassResoValue = (Reco_dilepton_M - Truth_dilepton_M)/Truth_dilepton_M
    for index in range(BinNumber):
        if xRangeList[index] < Truth_dilepton_M < xRangeList[index+1]:
            if IsMuonChannel == 1:
                print("W_reco: ",W_reco)
                MassResoHistList_uu[i].Fill(MassResoValue,W_reco)
            if IsElectronChannel == 1:
                MassResoHistList_ee[i].Fill(MassResoValue,W_reco)

c1 = TCanvas("c1","c1",800,600)
c1.cd()
gPad.SetTopMargin(0.09)
gPad.SetBottomMargin(0.10)
gPad.SetLeftMargin(0.10)
gPad.SetRightMargin(0.12)
MassResoHistList_uu[0].Draw()
c1.SaveAs("MassResoHistList_uu.png")
MassResoHistList_ee[0].Draw()
c1.SaveAs("MassResoHistList_ee.png")

Outfile_ee = TFile.Open("MassResoHist_ee.root","RECREATE")
Outfile_ee.cd()
for i in range(BinNumber):  
    MassResoHistList_ee[i].Write()
Outfile_ee.Close()

Outfile_uu = TFile.Open("MassResoHist_uu.root","RECREATE")
Outfile_uu.cd()
for i in range(BinNumber):  
    MassResoHistList_uu[i].Write()
Outfile_uu.Close()
        
        
        


